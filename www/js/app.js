// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var cirussageApp = angular.module('cirussageApp', ['ionic'])

.controller('FormController',function($scope,$http,$ionicLoading){

  $scope.ReportData={};

  
    $scope.insertIntoDynamoDBOnSubmit = function(){

      
      $ionicLoading.show({
         template: 'Processing Data...'
      });

      var proceed =false;
   
   function process_post(){

        $http({
                        url: "http://localhost:8081/process_post",
                        method: "POST",
                        dataType:'jsonp',
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.ReportData)

                    }).success(function(data, status, headers, config) {
                        $scope.status = status;
                       

                        console.log(data);
                    }).error(function(data, status, headers, config) {
                        $scope.status = status;
                        $ionicLoading.hide();
                        alert(status);
                         
                       
                    });


            }   

      var onSuccess = function(position) {

        $scope.ReportData.gpslocation = parseFloat(position.coords.longitude).toFixed(2) + ","+ parseFloat(position.coords.latitude).toFixed(2);
          process_post();
         
    };

    function onError(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }

      navigator.geolocation.getCurrentPosition(onSuccess, onError);
 
      //
   
    }

}); 




