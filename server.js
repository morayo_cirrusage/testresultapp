var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var aws =require('aws-sdk');

aws.config.update({region: 'eu-west-2'});

var docClient = new aws.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});

// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.post('/process_post', urlencodedParser, function (req, res) {

var params = {
  TableName: 'TestResult',
  Item: {
  	'TestResultID':req.body.testresuldId,
  	'SampleID':req.body.sampleid,
  	'TestDate':req.body.testdate,
  	'GpsLocation':req.body.gpslocation,
  	'State':req.body.state,
  	'Region':req.body.region,
  	'Client':req.body.client,
  	'StaffID':req.body.staffid,
  	'TestFacility':req.body.testfacility,
  	'TestPerformed':req.body.testperformed,
  	'Reading':req.body.reading,
  	'Unit':req.body.unit,
  	'TestFormula':req.body.testformula,
  	'ErrorCode':req.body.errorcode,
  	

  }
};

docClient.put(params, function(err, data) {
       if (err) {
           console.error("Unable to add result", req.body.gpslocation, ". Error JSON:", JSON.stringify(err, null, 2));
       } else {
           console.log("PutItem succeeded:", req.body);
       }
    });

   response = {
      
      last_name:req.body
   };
   console.log(req.body.gpslocation);
   res.end(JSON.stringify(response));
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   

})